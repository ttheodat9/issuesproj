//Initializing the search component on the select2s on the issues pages --- IMPORTANT
$(document).ready(() => {
    $('#responsible').select2({
    });
    $('#responsibleEdit').select2({
    });
})

//Displaying message onload for the About Page
aboutLoad = () => {
    $('#aboutModal').modal('show');
}

//Hiding the toast message on the technicians page

///TECHNICIANS PAGES
newTech = () => {
    //Checking the phone and email input
    var techPhone = document.getElementById('phone').value.trim();
    var techEmail = document.getElementById('email').value.trim();
    var digitCount = 0;
    var emailCount = 0;
    for (const digit in techPhone) {
        if (!isNaN(digit)) {
            digitCount++;
        }
    }
    if (techEmail.includes("@")) {
        emailCount++;

    }

    //Checking if form is incomplete
    if (document.getElementById('fname').value === '' ||
        document.getElementById('lname').value === '' ||
        document.getElementById('email').value === '' ||
        document.getElementById('phone').value === '') {
        swal('Incomplete Form!', 'All inputs must be filled.', 'warning');
    }
    //Checking for phone/email input error
    else if (digitCount < 10 || digitCount > 10 || emailCount != 1) {
        swal('Check your inputs!', 'Contact Number and Email inputs must be filled correctly.', 'error');
    }
    //Submitting complete, correct data 
    else {
        var techInfo = {};
        techInfo.techFirstname = $('#fname').val();
        techInfo.techLastname = $('#lname').val();
        techInfo.techEmail = $('#email').val();
        techInfo.techContact = $('#phone').val();

        $.ajax({
            type: "POST",
            url: "/addTech/save",
            data: techInfo,
            success: (data, status) => {
                if (status === 'success') {
                    swal('Accepted!', `Welcome to Teri\'s Issue Manager, ${techInfo.techFirstname} ${techInfo.techLastname}.`, 'success');
                    $('#fname').val('');
                    $('#lname').val('');
                    $('#email').val('');
                    $('#phone').val('');
                }
            }
        });
    }
}
editTech = (theTech) => {
    $('#fnameEdit').val(theTech.techFirstname);
    $('#lnameEdit').val(theTech.techLastname);
    $('#emailEdit').val(theTech.techEmail);
    $('#phoneEdit').val(theTech.techContact);
    $('#IDedit2').val(theTech.techID);
    $('#techModal').modal('show');

    $('#techEdits').on('click', () => {
        //Checking the phone and email input
        var techPhone = document.getElementById('phoneEdit').value.trim();
        var techEmail = document.getElementById('emailEdit').value.trim();
        var digitCount = 0;
        var emailCount = 0;
        for (const digit in techPhone) {
            if (!isNaN(digit)) {
                digitCount++;
            }
        }
        if (techEmail.includes("@")) {
            emailCount++;

        }

        //Checking if form is incomplete
        if (document.getElementById('fnameEdit').value === '' ||
            document.getElementById('lnameEdit').value === '' ||
            document.getElementById('emailEdit').value === '' ||
            document.getElementById('phoneEdit').value === '') {
            swal('Incomplete Form!', 'All inputs must be filled.', 'warning');
        }
        //Checking for phone/email input error
        else if (digitCount < 10 || digitCount > 10 || emailCount != 1) {
            swal('Check your inputs!', 'Contact Number and Email inputs must be filled correctly.', 'error');
        }
        //Submitting complete, correct data 
        else {
            var techUpdate = {
                techFirstname: $('#fnameEdit').val(),
                techLastname: $('#lnameEdit').val(),
                techEmail: $('#emailEdit').val(),
                techContact: $('#phoneEdit').val()
            };

            let editID = $('#IDedit2').val();
            $.ajax({
                type: "PUT",
                url: `/techListing/update/${editID}`,
                data: techUpdate,
                success: (data, status) => {
                    if (status === 'success') {
                        location.reload();
                    }
                }
            });
        }
    })
}
deleteTech = (techID) => {
    swal({
        title: "Are you sure you want to delete this record?",
        text: "Once deleted, this record cannot be recovered.",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {
                swal("This record has been deleted.", {
                    icon: "success",
                });

                $.ajax({
                    url: `/techListing/delete/${techID}`,
                    type: 'DELETE',
                    success: (data, status) => {
                        if (status === 'success') {
                            location.reload();
                        }
                    }
                })
            } else { $('.cancelled').toast('show'); }
        });
}



///ISSUES PAGES
newIssue = () => {
    //Checking if form is incomplete
    if (document.getElementById('description').value === '') {
        swal('Incomplete Form!', 'All inputs must be filled.', 'warning');
    }
    //Submitting complete, correct data 
    else {
        var issueInfo = {};
        issueInfo.issueDescription = $('#description').val();
        issueInfo.issueSeverity = $('#severity').val();
        issueInfo.issueStatus = $('#status').val();
        issueInfo.techID = $('#responsible').val();

        console.log(issueInfo.techID);

        $.ajax({
            type: "POST",
            url: "/addIssues/save",
            data: issueInfo,
            success: (data, status) => {
                if (status === 'success') {
                    swal('Submitted!', 'This issue has been posted.', 'info');
                    $('#description').val('');
                }
            }
        });
    }
}
editIssue = (theIssue) => {
    $('#descriptionEdit').val(theIssue.issueDescription);
    $('#severityEdit').val(theIssue.issueSeverity);
    $('#statusEdit').val(theIssue.issueStatus);
    $('#responsibleEdit').val(theIssue.techID).trigger('change'); //IMPORTANT -> Shows the already selected value in the select2
    $('#IDedit').val(theIssue.issueID);
    $('#issueModal').modal('show');


    $('#issueEdits').on('click', () => {
        //Checking if form is incomplete
        if (document.getElementById('descriptionEdit').value === '') {
            swal('Incomplete Form!', 'All inputs must be filled.', 'warning');
        }
        //Submitting complete, correct data  
        else {
            let techPerson = '';
            if ($('#responsibleEdit').val('')) {
                techPerson = theIssue.issueResponsible;
            } else {
                techPerson = $('#responsibleEdit').val();
            }
            var issueUpdate = {
                issueDescription: $('#descriptionEdit').val(),
                issueSeverity: $('#severityEdit').val(),
                issueStatus: $('#statusEdit').val(),
                techID: techPerson
            };

            let editID = $('#IDedit').val();
            $.ajax({
                type: "PUT",
                url: `/issueListing/update/${editID}`,
                data: issueUpdate,
                success: (data, status) => {
                    if (status === 'success') {
                        location.reload();
                    }
                }
            });
        }
    })
}
deleteIssue = (issueID) => {
    swal({
        title: "Are you sure you want to delete this record?",
        text: "Once deleted, this record cannot be recovered.",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {
                swal("This record has been deleted.", {
                    icon: "success",
                });

                $.ajax({
                    url: `/issueListing/delete/${issueID}`,
                    type: 'DELETE',
                    success: (data, status) => {
                        if (status === 'success') {
                            location.reload();
                        }
                    }
                })
            } else { $('.cancelled').toast('show'); }
        });
}


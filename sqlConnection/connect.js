const sql = require('mysql');

const connection = sql.createConnection({
    host: process.env.DBHOST,
    user: process.env.DBUSER,
    password: process.env.DBPASS,
    database: process.env.DATABASE,
    multipleStatements: true
})

module.exports = connection;
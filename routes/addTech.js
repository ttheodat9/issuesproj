const express = require('express');
const router = express.Router();
 
/* GET new technicans page. */
router.get('/', (req, res) => {
  res.render('addTech', { title: 'Add a Technician' });
});
  
router.post('/save', (req, res) => {
  let form = req.body;
  let saveCmd = 'INSERT INTO technicians SET ?';
  connection.query(saveCmd, form, (error, result) => {
    if (error) throw error;
    res.end();
  });
  res.end();
});

module.exports = router;
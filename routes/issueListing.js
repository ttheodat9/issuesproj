const express = require('express');
const router = express.Router();

/* GET new technicans page. */
router.get('/', (req, res) => {
    let lists = [
        'SELECT issues.*, technicians.techFirstname, technicians.techLastname  FROM issues INNER JOIN technicians ON issues.techID = technicians.techID',
        'SELECT * FROM technicians'
    ];
    connection.query(lists.join(';'), (error, result) => {
        if (error) throw error;
        res.render('issueListing', {
            issues: JSON.stringify(result[0]),
            technicians: JSON.stringify(result[1])
        });
    });
});

router.put('/update/:issueID', (req, res) => {
    let iID = req.params.issueID;
    let form2 = req.body;
    let updateCmd = `UPDATE issues SET ? WHERE issueID = ?`;
    connection.query(updateCmd, [form2, iID], (error, result) => {
        if (error) throw error;
        res.end();
    })
})

router.delete('/delete/:issueID', (req, res) => {
    let iID = req.params.issueID;
    let deleteCmd = 'DELETE FROM issues WHERE issueID = ?';
    connection.query(deleteCmd, iID, (error, result) => {
        if (error) throw error;
        res.end();
    })
})

module.exports = router;
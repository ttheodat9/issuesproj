const express = require('express');
const router = express.Router();

/* GET new issues page. */
router.get('/', (req, res) => {
  let twoQueries = [
    'SELECT issues.*, technicians.techFirstname, technicians.techLastname  FROM issues INNER JOIN technicians ON issues.techID = technicians.techID',
    'SELECT * FROM technicians'
  ];
  connection.query(twoQueries.join(';'), (error, result) => {
    if (error) throw error;
    res.render('addIssues', {
      issues: JSON.stringify(result[0]),
      technicians: JSON.stringify(result[1])
    });
  });
});
  
router.post('/save', (req, res) => {
  let form2 = req.body;
  let saveCmd = 'INSERT INTO issues SET ?';
  connection.query(saveCmd, form2, (error, result) => {
    if (error) throw error;
    res.end();
  });
  res.end();
});

module.exports = router;
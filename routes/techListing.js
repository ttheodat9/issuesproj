const express = require('express');
const router = express.Router();

/* GET new technicans page. */
router.get('/', (req, res) => {
    let listCmd = 'SELECT * FROM technicians';
    connection.query(listCmd, (error, result) => {
        if (error) throw error;
        res.render('techListing', {
            info: JSON.stringify(result)
        });
    });
});
router.put('/update/:techID', (req, res) => {
    let tID = req.params.techID;
    let form = req.body;
    let updateCmd = `UPDATE technicians SET ? WHERE techID = ?`;
    connection.query(updateCmd, [form, tID], (error, result) => {
        if (error) throw error;
        res.end(); 
    })
})

router.delete('/delete/:techID', (req, res) => {
    let tID = req.params.techID;
    let deleteCmd = 'DELETE FROM technicians WHERE techID = ?';
    connection.query(deleteCmd, tID, (error, result) => {
        if (error) throw error;
        res.end();
    })
})
module.exports = router;
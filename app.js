var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
require('dotenv').config();

var indexRouter = require('./routes/index');
var aboutRouter = require('./routes/about');
var techAddRouter = require('./routes/addTech');
var issueAddRouter = require('./routes/addIssues');
var techListRouter = require('./routes/techListing');
var issueListRouter = require('./routes/issueListing');
global.connection = require('./sqlConnection/connect');

connection.connect((error) => {
  if(error) throw error;
  console.log('MySQL is connected!');
});

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');
app.engine('html', require('ejs').renderFile);

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/about', aboutRouter);
app.use('/addTech', techAddRouter);
app.use('/addIssues', issueAddRouter);
app.use('/techListing', techListRouter);
app.use('/issueListing', issueListRouter);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
